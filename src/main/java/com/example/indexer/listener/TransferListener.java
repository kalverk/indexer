package com.example.indexer.listener;

import com.example.indexer.gateway.MetaDataGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.websocket.WebSocketService;
import org.web3j.utils.Numeric;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigInteger;
import java.net.ConnectException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Collections.emptyList;

@Component
public class TransferListener {

    private static final Logger logger = LoggerFactory.getLogger(TransferListener.class);

    //    TODO this can be replaces by a proper filter
    private static final Predicate<Log> IS_ERC_721_TRANSFER = log -> log.getTopics().size() == 4 && log.getTopics().stream().anyMatch(s -> s.equals("0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef"));

    private final WebSocketService web3jService;
    private final Web3j web3j;
    private final MetaDataGateway metaDataGateway;

    public TransferListener(MetaDataGateway metaDataGateway) {
        this.metaDataGateway = metaDataGateway;
//TODO move to properties
        this.web3jService = new WebSocketService("wss://mainnet.infura.io/ws/v3/c196f60fb27948a08604346c0deffa00", true);
        this.web3j = Web3j.build(web3jService);
    }

    @PostConstruct
    public void listen() throws ConnectException {
//        TODO handle reconnection
        web3jService.connect();
        web3j.ethLogFlowable(new EthFilter())
                .filter(IS_ERC_721_TRANSFER::test)
                .subscribe(this::logTransfer, throwable -> logger.error("Failed to subscribe to wss events", throwable));
    }

    private void logTransfer(Log log) throws IOException {
//        TODO handle call async
        var transactionReceipt = web3j.ethGetTransactionReceipt(log.getTransactionHash()).send();
        var to = getTo(transactionReceipt);
        var from = getFrom(transactionReceipt);

//        TODO these calls should be made in parallel
        var nameOfTheCollection = getNameOfTheCollection(from, to);
        var symbol = getSymbol(from, to);
        var tokenUri = getTokenUri(log.getTopics(), from, to);
        var tokenMetaData = metaDataGateway.fetchByTokenUri(tokenUri).orElse(null);
        logger.info(format("contract=%s, from=%s, to=%s, tokenURI=%s, symbol=%s, name of the collection=%s, meta data=%s", log.getAddress(), from, to, tokenUri, symbol, nameOfTheCollection, tokenMetaData));
    }

    private String getTo(EthGetTransactionReceipt transactionReceipt) {
        return transactionReceipt.getTransactionReceipt()
                .map(TransactionReceipt::getTo)
                .orElse(null);
    }

    private String getFrom(EthGetTransactionReceipt transactionReceipt) {
        return transactionReceipt.getTransactionReceipt()
                .map(TransactionReceipt::getFrom)
                .orElse(null);
    }

    private String getNameOfTheCollection(String from, String to) throws IOException {
        Function getName = new Function(
                "name",
                emptyList(),
                List.of(new TypeReference<Utf8String>() {
                })
        );
        return doEthCall(getName, from, to);
    }

    private String getSymbol(String from, String to) throws IOException {
        Function getSymbol = new Function(
                "symbol",
                emptyList(),
                List.of(new TypeReference<Utf8String>() {
                })
        );
        return doEthCall(getSymbol, from, to);
    }

    private String getTokenUri(List<String> topics, String from, String to) throws IOException {
        var bytes = Numeric.hexStringToByteArray(topics.get(3));
        Function getTokenUri = new Function(
                "tokenURI",
                List.of(new Uint256(new BigInteger(bytes))),
                List.of(new TypeReference<Utf8String>() {
                })
        );
        return doEthCall(getTokenUri, from, to);
    }

    private String doEthCall(Function function, String from, String to) throws IOException {
        String encodedFunction = FunctionEncoder.encode(function);
        var transaction = Transaction.createEthCallTransaction(from, to, encodedFunction);
//        TODO handle call async
        var ethCall = web3j.ethCall(transaction, DefaultBlockParameterName.LATEST).send();
        var decode = FunctionReturnDecoder.decode(ethCall.getValue(), function.getOutputParameters());
        return decode.stream()
                .map(Type::getValue)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }

}
