package com.example.indexer.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Optional.empty;

@Service
public class MetaDataGateway {

    private static final Logger logger = LoggerFactory.getLogger(MetaDataGateway.class);

    private final WebClient webClient;

    public MetaDataGateway(WebClient webClient) {
        this.webClient = webClient;
    }

    public Optional<TokenMetaData> fetchByTokenUri(String tokenUri) {
        if (isNull(tokenUri)) return empty();
        return webClient.get()
                .uri(tokenUri)
                .retrieve()
                .bodyToMono(TokenMetaData.class)
                .onErrorResume(throwable -> Mono.empty())
                .doOnError(throwable -> logger.error("Failed to fetch meta data for token=" + tokenUri, throwable))
                .blockOptional();
    }

}
